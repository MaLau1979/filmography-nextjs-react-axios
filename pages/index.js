import React, { useState } from "react"
import Head from "next/head"
import {
  Container,
  Typography,
  Card,
  Grid,
  TextField,
  Button,
} from "@material-ui/core"

import { Movie } from "@material-ui/icons"
import styles from "./style"

export default function Home() {
  const [searchText, setSearchText] = useState("")
  const classes = styles()

  const handleSearchTextChange = event => {
    setSearchText(event.target.value)
  }
  //console.log(searchText)

  const handleCleanTextClick = event => {
    console.log(10)
  }

  const handleSearchTextClick = event => {
    console.log(10)
  }

  return (
    <Container className={classes.container}>
      <Card className={classes.cardContainer}>
        <Grid container className={classes.titleGridContainer}>
          <Grid>
            <Typography className={classes.title}>Welcome!</Typography>
          </Grid>
          <Grid>
            <Movie className={classes.movieIcon} />
          </Grid>
        </Grid>
        <TextField
          value={searchText}
          placeholder="Buscar..."
          className={classes.textFieldSearch}
          onChange={handleSearchTextChange}
        />
        <Grid className={classes.buttonsContainer}>
          <Button variant="contained" onClick={handleCleanTextClick}>
            Limpiar
          </Button>
          <Button
            variant="contained"
            className={classes.searchButton}
            color="primary"
            size="large"
            onClick={handleSearchTextClick}
          >
            Buscar
          </Button>
        </Grid>
      </Card>
    </Container>
  )
}
